#!/usr/bin/env sh
git submodule init && git submodule update

VENDOR=$(lsb_release -is)

if [ "${VENDOR}" = "Debian" ]; then
	sudo apt-get install build-essential ruby-dev libcurl4-gnutls-dev bundler nodejs patch imagemagick
else
	sudo yum install -y ruby-devel rubygems-devel gcc-c++ curl-devel rubygem-bundler patch zlib-devel ImageMagick
fi

if [ -e vendor/bundle ]; then
	bundle update
else
	bundle install --path vendor/bundle
fi

